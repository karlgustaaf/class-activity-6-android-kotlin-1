package com.example.myfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var blShowHello=true

    override fun onCreate(savedInstanceState: Bundle?) {            //onCreate is the start to anything for this platform
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)                      //loads and executes visual element
        // tvTitle.setText("Good-bye sucker!")
    }


    fun clickBtn(v:View){                                                 //no parameter inserted, they're inserted here. We want the button to change the EVENT. We want that button to preform an action "on click".
        if(blShowHello==true){
            tvTitle.setText("Good-bye Sucker!")
            blShowHello=false
        }
        else{
            tvTitle.setText("Hello Sucker!")
            blShowHello=true
        }



    }

    fun loginclick(v:View){
        if (editText2.text.contains("Karl")==true && editText.text.contains("1234")==true){
            textView2.setText("Login Successful")
        }
        else{
            textView2.setText("WRONG")

        }

    }
    fun signupclick(v:View){



    }

}
